"""smartapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from .views import post_list
from django.views.generic import TemplateView



from smartapp.views import post_list,create_new,write,read,get_uid


urlpatterns = [
    url(r'^admin/', admin.site.urls), 
    url(r'^api/getuid',get_uid,name='get_uid'), 
    url(r'^api/createNew',create_new,name='create_new'), 
    url(r'^api/write',write,name='write'), 
    url(r'^api/read',read,name='read'), 
    url(r'^api/',post_list,name='post_list'), 
    
    
    url(r'^app/', TemplateView.as_view(template_name="index.html")),
    


]
