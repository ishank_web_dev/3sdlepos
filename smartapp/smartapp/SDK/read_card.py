from smartcard.System import readers
from Crypto.Cipher import AES 
from smartcard.util import toASCIIBytes,toASCIIString

from smartcard.util import PACK,hl2bs,bs2hl, BinStringToHexList,toBytes


r=readers()
print(r[0])

#####

IV = 16 * '\x00'           # Initialization vector: discussed later

key = "31323334353637383132333435363738"
key_as_binstring = hl2bs( toBytes( key ) )

KEYB="90 90 90 90 90 90"
READER_AUTH_APDU="FF 82 00 00 06 "




#####

#####


#####

def load_auth_key(connection):
	data,sw1,sw2=connection.transmit(toBytes(READER_AUTH_APDU+KEYB))
	#print(data,sw1,sw2,"loaded auth keys")

def decrypt_user_data(user_data):
	print("data",user_data)
	cipher = AES.new(key_as_binstring,AES.MODE_ECB )
	decrypted_as_string = cipher.decrypt(hl2bs(user_data))
	return decrypted_as_string

	
def authenticate(blockNo,connection):
	AUTHN=[0xFF,0x88,0x00,blockNo,0x60, 0x00]
	data,sw1,sw2=connection.transmit(AUTHN)
	#print(data,sw1,sw2,"auth")



def get_block_no(sector,block):
	#print("block no",(sector*4)+block)
	return (sector*4)+block

def get_user_data(blockNo,connection):
	READ=[0xFF, 0xB0,0x00,blockNo,0x10]
	data,sw1,sw2=connection.transmit(READ)
	print(data,sw1,sw2)
	return data



def getUId(connection):
	UID_APDU=[0xFF,0xCA,0x00,0x00,0x00]
	data,sw1,sw2=connection.transmit(UID_APDU)
	return data
def main_func(sector_no,block):
	connection=r[0].createConnection()
	connection.connect()
	uid=getUId(connection)
	load_auth_key(connection)	
	authenticate(get_block_no(sector_no,block),connection)
	user_data_en=get_user_data(get_block_no(sector_no,block),connection)
	print("user data = ",user_data_en)
	# user_data=decrypt_user_data(user_data_en)
	return toASCIIString(user_data_en),uid